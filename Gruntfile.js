module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        xconfig: grunt.file.readJSON('grunt.config.json'),
        jsfiles: [
            'src/procentric_core.js',
            'src/procentric_debug.js',
            'src/procentric_device.js',
            'src/procentric_player.js',
            'src/procentric_power.js',
            'src/procentric_api.js'
        ],

        copy: {
            lib: {
                files: [
                    {expand: true, cwd: 'lib/', src: ['**'], dest: 'bin/'},
                    {expand: true, cwd: 'bin/', src: ['**'], dest: 'test/public/javascripts/'}
                ],
            },

            local: {
                files: [
                    {
                        expand: true,
                        cwd: 'bin/',
                        src: ['**'],
                        dest: '<%= xconfig.env.local %>/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: 'bin/',
                        src: ['**'],
                        dest: '<%= xconfig.env.ecb.local %>/',
                        filter: 'isFile'
                    }
                ]
            },

            lab: {
                files: [
                    {
                        expand: true,
                        cwd: 'bin/',
                        src: ['**'],
                        dest: '<%= xconfig.env.lab %>/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: 'bin/',
                        src: ['**'],
                        dest: '<%= xconfig.env.ecb.lab %>/',
                        filter: 'isFile'
                    }
                ]
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                footer: '\nProcentric.VERSION = "<%= pkg.version %>";\n'
            },
            js: {
                options: {
                    sourceMap: true,
                    sourceMapName: 'bin/procentric.map'
                },
                files: {
                    'bin/procentric.min.js': '<%= jsfiles %>'
                }
            },
            debug: {
                options: {
                    sourceMap: true,
                    sourceMapName: 'bin/procentric.map',
                    mangle: false,
                    compress: false,
                    beautify: true
                },
                files: {
                    'bin/procentric.min.js': '<%= jsfiles %>'
                }
            }
        },

        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                pushTo: 'master',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-bump');

    grunt.registerTask('build', ['uglify:js', 'copy']);
    grunt.registerTask('build.local', ['uglify:js', 'copy:lib', 'copy:local']);
    grunt.registerTask('build.lab', ['uglify:js', 'copy:lib', 'copy:lab']);
    grunt.registerTask('debug', ['uglify:debug', 'copy']);
    grunt.registerTask('debug.local', ['uglify:debug', 'copy:lib', 'copy:local']);
    grunt.registerTask('debug.lab', ['uglify:debug', 'copy:lib', 'copy:lab']);
};