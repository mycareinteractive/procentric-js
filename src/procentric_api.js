Procentric = Procentric || {};

/**
 * Enable/disable debug mode
 * @param enabled
 */
Procentric.setDebug = function (enabled) {
    this.isDebug = enabled;
};

/**
 * Initialize Pro:Centric object asynchronously
 * @param scallback
 * @param fcallback
 */
Procentric.bootstrap = function (scallback, fcallback, pcallback) {

    if(!window.LG) {
        debug.log('LG device not detected, bootstrap failed.');
        fcallback.apply();
        return;
    }

    debug.log('...initializing procentric');

    this.preload = {
        model: {
            name: "Model",
            method: this.fetchModelName,
            completed: false
        },
        version: {
            name: "Version",
            method: this.fetchPlatformVersion,
            completed: false
        },
        power: {
            name: "Power State",
            method: this.Power.fetchState,
            completed: false
        },
        channel: {
            name: "Start Channel",
            method: this.fetchStartChannel,
            completed: false
        },
        appList: {
            name: "Application List",
            method: this.fetchApplicationList,
            completed: false
        },
        netDeviceCount: {
            name: "Network Device Count",
            method: this.fetchNetworkDeviceCount,
            completed: false
        }
    };

    this.successCallback = scallback;
    this.failedCallback = fcallback;
    this.progressCallback = pcallback;

    this.executePreload();

    this.loadCheck(this);
};

Procentric.executePreload = function() {
    for(var p in this.preload) {
        this.preload[p]['method'].apply(Procentric);
    }
}

/**
 *
 * @returns {string}
 */
Procentric.getModelName = function () {
    return this.modelName;
};

/**
 *
 * @returns {string}
 */
Procentric.getPlatformVersion = function () {
    return this.platformVersion;
};

Procentric.getPlatform = function () {
    return this.platform;
};

Procentric.getVersion = function () {
    return this.version;
};

Procentric.getTimeZoneName = function () {
    return this.timeZoneName;
};

Procentric.getApplicationList = function () {
    return this.applicationList;
};

Procentric.getNetworkDeviceCount = function () {
    return this.networkDeviceCount;
};

Procentric.getNetworkDeviceList = function () {
    return this.networkDeviceList;
};

Procentric.getNetworkDevice = function (id) {
    return this.networkDeviceList[id];
};

Procentric.playChannel = function (obj) {
    if(obj && obj.type && obj.type.toUpperCase() == 'IP')
        return this.tuneIPChannel(obj);
    else if(obj && obj.type && obj.type.toUpperCase() == 'LOGICAL')
        return this.tuneLogicalChannel(obj);
    else
        return this.tuneRFChannel(obj);
};

Procentric.stopChannel = function () {
    this.killChannel();
};

/**
 * Force set local time on LG device
 * @param timeStr The string format of the local time
 */
Procentric.setTime = function (d) {

    var objTime = {
        year: d.year,
        month: d.month,
        day: d.day,
        hour: d.hour,
        minute: d.minute,
        second: d.second,
        gmtOffsetInMinute: d.gmtOffsetInMinute,
        isDaylightSaving: d.isDaylightSaving,
        onSuccess: function () {
            debug.log('Time successfully set on the LG device');
        },
        onFailure: function () {
            debug.log('FAILED to set the time on the LG device');
        }
    };

    hcap.time.setLocalTime(objTime);
};

Procentric.setPowerOnTime = function(time) {
    Procentric.Power.setPowerOnTime(time);
};

Procentric.getPower = function() {
    return Procentric.Power.getLocalState();
};

Procentric.setPower = function(state) {
    Procentric.Power.setState(state);
};

