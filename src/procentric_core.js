"use strict";
var Procentric = {

    errors: [],
    isLoaded: false,
    isDebug: false,

    modelName: "",
    platformVersion: "",
    platform: "LG PROCENTRIC",
    version: "PROCENTRIC",
    timeZoneName: "",
    networkDeviceList: [],
    applicationList: [],
    networkDeviceCount: "",
    startChannel: {},

    Power: {},

    successCallback: null,
    failedCallback: null,
    progressCallback: null,
};

// We can detect user-agent before DOM ready.  If it's LG let's assign window.LG for later use.
if (navigator.userAgent.indexOf(';LGE ;') > -1) {
    window.LG = Procentric;
}

// Dummy debug object
var dummyDebug = {};
dummyDebug.log = dummyDebug.trace = dummyDebug.status = function(msg) {
    console.log(msg);
};
var debug = window.debug || dummyDebug;

Procentric.loadCheck = function (inst) {
    if (inst.getIsLoaded() == false) {
        setTimeout(function () {
            inst.loadCheck(inst);
        }, 100);
    } else {
        inst.successCallback();
        debug.log('BOOTSTRAPPED Instance!');
        debug.log("Load Total: " + inst.loadTotal);
        debug.log("Loaded: " + inst.countLoaded);
        debug.log('Procentric initialize complete');
    }
};

Procentric.getIsLoaded = function () {
    this.isLoaded = true;

    for(var p in this.preload) {
        if(!this.preload[p].completed) {
            this.isLoaded = false;
            return this.isLoaded;
        }
    }

    return this.isLoaded;
};

Procentric.setErrorCount = function () {
    this.errorCount = this.errorCount + 1;
};

Procentric.setError = function (method, message) {
    this.errors.push({
        method: method,
        message: message
    });
};

Procentric.setProperty = function(key, val){
    hcap.property.setProperty({
        key: key,
        value: val,
        "onSuccess":function() {
            console.log("[Procentric.setProperty]: Successfully set " + key + " = " + val);
        },
        "onFailure":function(f) {
            console.log("[Procentric.setProperty]: FAILED to set property " + key + " = " + val + " [ errorMessage = " + f.errorMessage + " ]");
        }
    });
}

Procentric.getError = function () {
    return this.errors;
};

Procentric.getStartChannel = function() {
    return this.startChannel;
},

Procentric.setPlatformVersion = function (res) {
    this.platform = res.value;
    this.platformVersion = res.value;
    this.countLoaded = this.countLoaded + 1;
    debug.log('Platform: ' + this.platform);
};

Procentric.setNetworkDeviceCount = function (res) {
    this.networkDeviceCount = res.count;
    this.loadTotal = this.loadTotal + (res.count - 1);
    debug.log('Net Device Count: ' + this.networkDeviceCount);
};

Procentric.setModelName = function (res) {
    this.modelName = res.value;
    debug.log('Model Name: ' + this.modelName);
};

Procentric.setNetworkDevice = function (res) {
    // if(typeof this.networkDeviceList !== "array")
    //     this.networkDeviceList = new Array();

    if (typeof res === 'object' && res !== null) {
        try {
            var dev = new Procentric.Device(res);
            this.networkDeviceList.push(dev);
            debug.trace("DEV IP: " + dev.getIp());
        } catch (e) {
            debug.log(e);
        }
        debug.log("Network Mode: " + res.networkMode + " Name: " + res.name + " MAC: " + res.mac);
    }
};

Procentric.setTimeZoneName = function (res) {
    this.timeZoneName = res.gmtOffsetInMinute;
    debug.log("Time Zone: " + this.timeZoneName);
};

Procentric.setApplicationList = function (res) {
    this.applicationList = res.list;

    var li = [];
    var ul = $("<ul />");
    for (var i = 0; i < res.list.length; i++) {
        var inf = $("<li />").append([res.list[i].id, res.list[i].title, res.list[i].iconFilePath].join(','));
        ul.append(inf);
    }

    debug.log(ul);
};

Procentric.setStartChannel = function(obj) {
    debug.log('[Procentric.setStartChannel]: The Start Channel object has been saved');
    this.startChannel = obj;
},

Procentric.fetchModelName = function () {
    var self = this;
    hcap.property.getProperty({
        key: "model_name",
        onSuccess: function (res) {
            var msg = "[Procentric.fetchModelName SUCCESS]: We successfully retrieved the Model Name: " + res;
            self.setModelName(res);
            self.preload.model.completed = true;
            self.progressCallback(self.preload.model.name);
            debug.log(msg);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchModelName FAIL]: We failed to retrieve the Model Name. errorMessage: " + res.errorMessage;
            self.setError('fetchModelName', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.model.name);
            debug.log(msg);
        }
    });
};

Procentric.fetchNetworkDeviceCount = function () {
    var self = this;
    debug.log('Fetching Network Device Count');
    hcap.network.getNumberOfNetworkDevices({
        onSuccess: function (res) {
            var msg = "[Procentric.fetchNetworkDeviceCount SUCCESS]: We successfully retrieved the Network Device Count: " + res.count;
            self.setNetworkDeviceCount(res);
            self.fetchNetworkDeviceList();
            self.preload.netDeviceCount.completed = true;
            self.progressCallback(self.preload.netDeviceCount.name);
            debug.log(msg);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchNetworkDeviceCount FAIL]: We failed to retrieved the Network Device Count. errorMessage" + res.errorMessage;
            self.setError('fetchNetworkDeviceCount', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.netDeviceCount.name);
            debug.log(msg);
        }
    });
};

Procentric.fetchNetworkDeviceList = function () {
    var self = this;
    var count = self.networkDeviceCount;
    if (count > 0) {
        for (var i = 0; i < count; i++) {
            self.fetchNetworkDevice(i);
            self.preload['nd' + i] = {
                name: "Network Device " + i,
                completed: false,
            };
        }
    }
};

Procentric.fetchNetworkDevice = function (index) {
    var self = this;
    debug.log('Fetching Network Device');
    hcap.network.getNetworkDevice({
        index: index,
        onSuccess: function (res) {
            var msg = "[Procentric.fetchNetworkDevice SUCCESS]: We successfully retrieved the Network Device: " + res.name;
            self.setNetworkDevice(res);
            self.preload['nd' + index].name = res.name;
            self.preload['nd' + index].completed = true;
            self.progressCallback('nd' + index + ': ' + self.preload['nd' + index].name);
            debug.log(msg);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchNetworkDevice FAIL]: We failed to retrieve the Network Device. errorMessage " + res.errorMessage;
            self.setError('fetchNetworkDevice', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload['nd' + index].name);
            debug.log(msg);
        }
    });
};

Procentric.fetchTimeZoneName = function () {
    var self = this;
    debug.log('Fetching Timezone From TV');
    hcap.time.getLocalTime({
        onSuccess: function (res) {
            var msg = "[Procentric.fetchTimeZoneName SUCCESS]: We successfully retrieved the Time Zone Name";
            self.setTimeZoneName(res);
            self.preload.time.completed = true;
            self.progressCallback(self.preload.time.name);
            debug.log(msg);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchTimeZoneName FAIL]: We failed to fetch the Time Zone Name. errorMessage: " + res.errorMessage;

            self.setError('fetchTimeZoneName', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.time.name);
            debug.log(msg);
        }
    });
};

Procentric.fetchApplicationList = function () {
    var self = this;
    debug.log('Fetching Application List');
    hcap.preloadedApplication.getPreloadedApplicationList({
        onSuccess: function (res) {
            var msg = "[Procentric.fetchApplicationList SUCCESS]: We successfully retrieved the Application List. errorMessage: ";
            self.setApplicationList(res);
            self.preload.appList.completed = true;
            self.progressCallback(self.preload.appList.name);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchApplicationList FAIL]: We failed to fetch the Application List. errorMessage: " + res.errorMessage;
            self.setError('fetchApplicationList', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.appList.name);
            debug.log(msg);
        }
    });
};

Procentric.fetchStartChannel = function() {
    var self = this;

    hcap.channel.getStartChannel({
        onSuccess:function(s) {
            self.setStartChannel(s);
            self.preload.channel.completed = true;
            self.progressCallback(self.preload.channel.name);
            debug.log("[Procentric.fechStartChannel SUCCESS]: Successfully retrieved Start Channel object: " +
                "\n channel type      : " + s.channelType     +
                "\n logical number    : " + s.logicalNumber   +
                "\n frequency         : " + s.frequency       +
                "\n program number    : " + s.programNumber   +
                "\n major number      : " + s.majorNumber     +
                "\n minor number      : " + s.minorNumber     +
                "\n satellite ID      : " + s.satelliteId     +
                "\n polarization      : " + s.polarization    +
                "\n rf broadcast type : " + s.rfBroadcastType +
                "\n ip                : " + s.ip              +
                "\n port              : " + s.port            +
                "\n ip broadcast type : " + s.ipBroadcastType +
                "\n symbol rate       : " + s.symbolRate      +
                "\n pcr pid           : " + s.pcrPid          +
                "\n video pid         : " + s.videoPid        +
                "\n video stream type : " + s.videoStreamType +
                "\n audio pid         : " + s.audioPid        +
                "\n audio stream type : " + s.audioStreamType +
                "\n signal strength   : " + s.signalStrength  +
                "\n source address    : " + s.sourceAddress);
        },
        "onFailure":function(f) {
            var msg = "[Procentric.fechStartChannel FAIL]: We were not able to retrieve the Start Channel: onFailure : errorMessage = " + f.errorMessage;

            // setting to true because it will fail if not initially set. typical during install
            self.preload.channel.completed = true;

            Procentric.setError('Procentric.fetchStartChannel', f.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.channel.name);
            debug.log(msg);
        }
    })
}

Procentric.fetchPlatformVersion = function () {
    var self = this;
    hcap.property.getProperty({
        key: "platform_version",
        onSuccess: function (res) {
            var msg = "[Procentric.fetchPlatformVersion SUCCESS]: We successfully fetched the Platform Version.";
            self.setPlatformVersion(res);
            self.preload.version.completed = true;
            self.progressCallback(self.preload.version.name);
            debug.log(msg);
        },
        onFailure: function (res) {
            var msg = "[Procentric.fetchPlatformVersion FAIL]: We failed to fetch the Platform Version. errorMessage: " + res.errorMessage;
            self.setError('fetchPlatformVersion', res.errorMessage);
            self.progressCallback('FAIL: ' + self.preload.version.name);
            debug.log(msg);
        }
    });
};

Procentric.executeBrowser = function () {
};



