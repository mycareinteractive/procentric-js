Procentric = Procentric || {};
Procentric.dbgKeybindings = function () {
    /*if(!this.isDebug)
     return;*/

    document.addEventListener('keydown', function (e) {
        var kc = e.keyCode;
        var hkc = hcap.key.Code;

        debug.log("Key Code: " + kc);

        switch (kc) {
            case hkc.RED:
                var d = new Date();
                location.href = 'index.html?time=' + d.getTime();
                break;

            case hkc.GREEN:
                hcap.mode.getHcapMode({
                    onSuccess: function (s) {
                        debug.log('HCAP MODE: ' + s.mode);
                    },
                    onFailure: function (f) {
                    }
                });
                break;

            case hkc.BLUE:
                $('body').toggle();
                break;

            case hkc.YELLOW:

                setTimeout(function () {
                    debug.log('going down for reboot now');
                }, 3000);

                hcap.power.reboot({
                    "onSuccess": function () {
                        debug.log("onSuccess");
                    },
                    "onFailure": function (f) {
                        debug.log("onFailure : errorMessage = " + f.errorMessage);
                    }
                });
                break;


            case hkc.PORTAL:
                hcap.mode.setHcapMode({
                    mode: hcap.mode.HCAP_MODE_1,
                    onSuccess: function () {
                        debug.log("onSuccess");
                    },
                    onFailure: function (f) {
                        debug.log("onFailure : errorMessage = " + f.errorMessage);
                    }
                });
                break;

        }
    });

};