Procentric = Procentric || {};
Procentric.Device = function(params){
    this.networkMode = "wired"; //wired or wireless
    this.name = ""; // name of the network device ex: eth0
    this.mac = "";
    this.ip = "";
    this.gateway = "";
    this.netmask = "";

    this.init(params);
};

Procentric.Device.prototype.init = function(params){
    if(typeof params == 'object'){
        for(var p in params){
            this[p] = params[p];
            debug.log("DEVICE PARAM INDEX: " + p + ": " + this[p]);
        }
    }
};

Procentric.Device.prototype.getNetworkMode = function(){
    return this.networkMode;
};

Procentric.Device.prototype.getName = function(){
    return this.name;
};

Procentric.Device.prototype.getMac = function(){
    debug.log('RETURNING MAC: ' + this.mac);
    return this.mac;
};

Procentric.Device.prototype.getIp = function(){
    return this.ip;
};

Procentric.Device.prototype.getGateway = function(){
    return this.gateway;
};

Procentric.Device.prototype.getNetmask = function(){
    return this.netmask;
};
