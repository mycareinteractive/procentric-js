Procentric = Procentric || {};
Procentric.tuneRFChannel = function (obj) {
    debug.log('CHANGING RF CHANNEL');
    hcap.channel.requestChangeCurrentChannel({
        channelType: hcap.channel.ChannelType.RF,
        majorNumber: obj.major,
        minorNumber: obj.minor,
        rfBroadcastType: hcap.channel.RfBroadcastType.CABLE,
        onSuccess: function () {
            debug.log('[Procentric.tuneRFChannel] We have tuned the channel. Successful RF channel tuning: ' + obj.major + '-' + obj.minor);
            if (obj.onSuccess) {
                obj.onSuccess.apply(obj);
            }
        },
        onFailure: function (f) {
            debug.log('[Procentric.tuneRFChannel] Failed to tune RF channel: errorMessage = ' + f.errorMessage);
            if (obj.onFailure) {
                obj.onFailure.apply(obj, arguments);
            }
        }
    });
};

Procentric.tuneLogicalChannel = function (obj) {
    debug.log('CHANGING LOGICAL CHANNEL');
    hcap.channel.requestChangeCurrentChannel({
        channelType: hcap.channel.ChannelType.RF,
        logicalNumber: obj.logical,
        rfBroadcastType: hcap.channel.RfBroadcastType.CABLE,
        onSuccess: function () {
            debug.log('[Procentric.tuneLogicalChannel] We have tuned the channel. Successful LOGICAL channel tuning: ' + obj.logical);
            if (obj.onSuccess) {
                obj.onSuccess.apply(obj);
            }
        },
        onFailure: function (f) {
            debug.log('[Procentric.tuneLogicalChannel] Failed to tune LOGICAL channel: errorMessage = ' + f.errorMessage);
            if (obj.onFailure) {
                obj.onFailure.apply(obj, arguments);
            }
        }
    });
};

Procentric.tuneIPChannel = function (obj) {
    debug.log('CHANGING IP CHANNEL');
    hcap.channel.requestChangeCurrentChannel({
        channelType: hcap.channel.ChannelType.IP,
        ip: obj.ip,
        port: obj.port,
        ipBroadcastType: hcap.channel.IpBroadcastType.UDP,
        onSuccess: function () {
            debug.log('[Procentric.tuneIPChannel] We have tuned the channel. Successful IP channel tuning: ' + obj.ip + '-' + obj.port);
            if (obj.onSuccess) {
                obj.onSuccess.apply(obj);
            }
        },
        onFailure: function (f) {
            debug.log('[Procentric.tuneIPChannel] Failed to tune IP channel: errorMessage = ' + f.errorMessage);
            if (obj.onFailure) {
                obj.onFailure.apply(obj, arguments);
            }
        }
    });
};

Procentric.killChannel = function () {
    var sc = Procentric.getStartChannel();

    hcap.channel.stopCurrentChannel({
        onSuccess: function () {
            debug.log('[Procentric.killChannel] We have stopped the channel');
            Procentric.tuneRFChannel({
                major: sc.majorNumber,
                minor: sc.minorNumber
            });
        },
        onFailure: function (f) {
            debug.log('[Procentric.killChannel] We FAILED to stop the channel errorMessage: ' + f.errorMessage);
        }
    });
}
