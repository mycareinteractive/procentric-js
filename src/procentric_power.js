Procentric = Procentric || {};

// Dummy debug object
var dummyDebug = {};
dummyDebug.log = dummyDebug.trace = dummyDebug.status = function(msg) {
    console.log(msg);
};
var debug = window.debug || dummyDebug;

Procentric.Power = {
    instantPower: 2
};

document.addEventListener(
    "power_mode_changed",
    function () {
        debug.log("[Procentric.Power]: Event 'power_mode_changed' is received POWER MODE HAS BEEN CHANGED!!! ");
        Procentric.stopChannel();
        hcap.power.getPowerMode({
            "onSuccess": function (s) {

                debug.log("[Procentric.Power]: We have checked the power mode and it is: " + s.mode);

                hcap.mode.setHcapMode({
                    mode: hcap.mode.HCAP_MODE_1,
                    onSuccess: function () {
                        debug.log("[Procentric.Power]: The TV successfully completed 'power_mode_change' event");
                    },
                    onFailure: function (f) {
                        debug.log("[Procentric.Power]: Failed to execute power_mode_change. onFailure : errorMessage = " + f.errorMessage);
                    }
                });

                var event = new Event('keydown');
                event.keyCode = event.Code = 16777399;
                Procentric.Power.setLocalState(s.mode);
                document.dispatchEvent(event);

                if (s.mode == hcap.power.PowerMode.NORMAL) {
                    if (Procentric.isDebug == true) {
                        var d = new Date();
                        location.href = 'index.html?time=' + d.getTime();
                    }
                }
            },
            "onFailure": function (f) {
                console.log("[Procentric.Power]: We were not able to retrieve the Power Mode: errorMessage = " + f.errorMessage);
            }
        });
    },
    false
);

Procentric.Power.setLocalState = function(status) {
    if(status == hcap.power.PowerMode.NORMAL) {
        this.state = 'normal';
        debug.log('[Procentric.Power]: Power State is NORMAL');
    } else if(status == hcap.power.PowerMode.WARM) {
        this.state = 'warm';
        debug.log('[Procentric.Power]: Power State is WARM');
    } else {
        this.state = 'off';
        debug.log('[Procentric.Power]: Power State is OFF');
    }
};

Procentric.Power.getLocalState = function() {
    return this.state;
};

Procentric.Power.fetchState = function() {
    var self = Procentric.Power;
    hcap.power.getPowerMode({
        "onSuccess":function(s) {
            self.setLocalState(s.mode);
            Procentric.preload.power.completed = true;
            Procentric.progressCallback(Procentric.preload.power.name);
            debug.log("[Procentric.Power.fetchState SUCCESS]: power mode " + self.state);
        },
        "onFailure":function(f) {
            var msg = "[Procentric.Power.fetchState FAIL]: Failed to retrieve power status. errorMessage = " + f.errorMessage;
            Procentric.setErrorCount();
            Procentric.setError('Power.fetchState', f.errorMessage);
            Procentric.progressCallback('FAIL: ' + Procentric.preload.power.name);
            debug.log(msg);
        }
    });
    hcap.property.getProperty({
        key: "instant_power",
        onSuccess: function (res) {
            self.instantPower = res.value;
            debug.log("[Procentric.Power.fetchState SUCCESS]: instant power = " + res.value);
        },
        onFailure: function (f) {
            debug.log("[Procentric.Power.fetchState FAIL]: unable to get instant power");
        }
    });

};


Procentric.Power.setState = function(state) {
    var self = this;
    state = state.toUpperCase();
    var pstate = hcap.power.PowerMode[state];
    if(this.instantPower != 0) {
        hcap.power.setPowerMode({
            mode: pstate,
            onSuccess: function () {
                self.setLocalState(pstate);
                console.log("[Procentric.Power.setState]: We have set the power mode to: " + state);
            },
            onFailure: function (f) {
                console.log("[Procentric.Power.setState]: We were NOT able to set the power mode to: " + state + " onFailure : errorMessage = " + f.errorMessage);
            }
        });
    }
    else if(this.instantPower == 0 && state != 'normal') {
        hcap.power.powerOff({
            onSuccess: function () {
                self.setLocalState(pstate);
                console.log("[Procentric.Power.setState]: We have powered off the device");
            },
            onFailure: function (f) {
                console.log("[Procentric.Power.setState]: We were NOT able to power off the device. onFailure : errorMessage = " + f.errorMessage);
            }
        });
    }
};

Procentric.Power.setPowerOnTime = function(time){
    var self = this;

    var when = time.split(':');

    hcap.time.setPowerOnTime({
        hour: Number(when[0]),
        minute: Number(when[1]),
        onSuccess:function() {
            debug.log("[Procentric.Power.setOnTime]: We have set the onTime to " + time);
        },
        onFailure:function(f) {
            debug.log("[Procentric.Power.setOnTime]: We were note able to set the Power on time to " + time + ". onFailure : errorMessage = " + f.errorMessage);
        }
    });

};